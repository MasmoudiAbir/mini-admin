<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use XLSXWriter; 

class ClientController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Affichage de la liste des clients avec le nombre de ses commandes
     *
     * @return void
     */ 
    public function index()
    {
        $data['clients'] = Client::withCount('commandes')->get();    
        return view('clients', $data);    
    }

    /**
     * export du fichier excel qui contient la liste des clients
     * et les détails des commandes de chaque client sur de feuille
     *
     * @return void
     */
    public function export()
    {
        include_once("XLSXWriter.php");
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        //nom du fichier xlsx
        $filename = "clients-et-commandes.xlsx";

        //entete du fichier
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        //premier page pour la liste des clients
        $headerClients = array(
            'Liste des clients' => 'string'
        );

        $clients = Client::withCount('commandes')->get();

        $listClient[$key]['prenom']  = 'Prénom';
        $listClient[$key]['nom']    = 'Nom';
        $listClient[$key]['email']  = 'Email';
        $listClient[$key]['telephone']  = 'Téléphone';
        $listClient[$key]['nombre_de_commande']  = 'nombre_de_commande';

        foreach ($clients as $key => $client) {
            $listClient[$key]['prenom']  = $client->prenom;
            $listClient[$key]['nom']    = $client->nom;
            $listClient[$key]['email']  = $client->email;
            $listClient[$key]['telephone']  = $client->telephone;
            $listClient[$key]['nombre_de_commande']  = $client->commandes_count;
        }

        $writer = new XLSXWriter();

        //page pour chaque client contient la liste des commandes avec leurs articles
        $writer->writeSheetHeader('Clients', $headerClients);
        foreach($listClient as $row)
          $writer->writeSheetRow('Clients', $row);

        $clientsWithCommanades = Client::with('commandes', 'commandes.articles')->get();

        foreach($clientsWithCommanades as $keyC => $client) {
            $clientCommandes = [];
            ${'headerClientName'.$keyC} = array(
                "Commandes du Client $client->prenom $client->nom" => 'string'
            );
            $clientCommandes[$keyC]['date_commande'] = 'Date de la commande';
            $clientCommandes[$keyC]['num_commande'] = 'Numéro de la commande';
            $clientCommandes[$keyC]['articles'] = 'Articles';
            $clientCommandes[$keyC]['prix'] = 'Prix total de la commande';

            foreach($client->commandes as $keyCmd => $commande) {
              $clientCommandes[$keyCmd]['date_commande'] = $commande->date_commande;
              $clientCommandes[$keyCmd]['num_commande'] = $commande->num_commande;

              $listArticle = null;
              $prix = 0;
              foreach($commande->articles as $article) {
                $listArticle .= $article->quantite.'x '.$article->nom.' Ref ('.$article->reference.') ';
                $prix += $article->quantite * $article->prix_unitaire;
              }
              $clientCommandes[$keyCmd]['articles'] = $listArticle;
              $clientCommandes[$keyCmd]['prix'] = $prix;
            }

            $writer->writeSheetHeader("$client->prenom $client->nom", ${'headerClientName'.$keyC});

            foreach($clientCommandes as $cwc){
              $writer->writeSheetRow("$client->prenom $client->nom", $cwc);
            }
        }  
             
        $writer->writeToStdOut();
        exit(0);
    }
 
}