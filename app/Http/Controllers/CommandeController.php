<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use Illuminate\Http\Request;

class CommandeController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }
  /**
   * Affichage de la liste des commandes
   *
   * @return void
   */ 
  public function index()
  {
    $data['commandes'] = Commande::with('articles')->get();    
    return view('commandes', $data);
  }
}