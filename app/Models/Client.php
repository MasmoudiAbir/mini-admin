<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'prenom',
        'nom',
        'email',
        'telephone'
    ];

    /**
     * Tout les commandes du client
     */
    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }
}
