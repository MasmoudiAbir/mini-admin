<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    
    protected $fillable = [
        'nom',
        'reference',
        'quantite',
        'prix_unitaire'
    ];


    public function commandes()
    {
        return $this->belongsToMany(Commande::class, 'commande_articles');
    }
}
