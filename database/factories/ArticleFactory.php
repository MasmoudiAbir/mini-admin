<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->word(10),
            'reference' => $this->faker->regexify('[A-Za-z0-9]{10}'),
            'quantite' => $this->faker->randomDigitNot(0),
            'prix_unitaire' => $this->faker->randomFloat(2, null, 100)
        ];
    }
}
