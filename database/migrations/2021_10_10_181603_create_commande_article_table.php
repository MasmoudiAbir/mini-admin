<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandeArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commande_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('commande_id');
            $table->unsignedInteger('article_id');
            $table->timestamps();
        });
        
        Schema::table('commande_articles', function (Blueprint $table) {
            $table->foreign('commande_id')->references('id')->on('commandes');            
            $table->foreign('article_id')->references('id')->on('articles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('commande_articles_commande_id_foreign');
        $table->dropForeign('commande_articles_article_id_foreign');        
        Schema::dropIfExists('commande_articles');
    }
}
