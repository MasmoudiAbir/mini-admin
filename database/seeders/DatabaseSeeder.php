<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Client::factory(100)->create();
        \App\Models\Commande::factory(100)->create();
        \App\Models\Article::factory(10)->create();
        \App\Models\CommandeArticle::factory(10)->create();
    }
}
