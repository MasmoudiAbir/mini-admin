<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\ClientController::class, 'index']);
Auth::routes();
Route::get('logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('client');
})->name('logout');
Route::get('/client', [App\Http\Controllers\ClientController::class, 'index']);
Route::get('/commande', [App\Http\Controllers\CommandeController::class, 'index']);
Route::get('/export', [App\Http\Controllers\ClientController::class, 'export']);

