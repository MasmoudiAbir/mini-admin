## Installation

- Clone the project
- Run "composer install"
- Run php artisan migrate
- Run php artisan db:seed
- Run the server "php artisan serve" or "php -S localhost:8000 -t public"
- Go to "http://127.0.0.1:8000"

## Principales dossiers

- "app/Http/Controllers" -> Code source des appelles REST API
- "app/resources/views" -> Contient les vues
- "app/routes/web.php" -> Contient les Urls utilissé sur le projet

