@extends('layouts.admin')

@section('content')
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">Commandes</h3>
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>                                                
                                    <th>Date de la commande</th>
                                    <th>Numéro de la commande</th>
                                    <th>Nombre d'articles</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>                                
                                @foreach($commandes as $commande)
                                <tr class="tr-shadow">                                                
                                    <td>{{ $commande->date_commande }}</td>
                                    <td>{{ $commande->num_commande }}</td>
                                    <td>{{ count($commande->articles) }}</td>                                                                         
                                    <td></td>
                                </tr>

                                @if(count($commande->articles) > 0)
                                    <tr>
                                        <td colspan="4">
                                        <div class="title--sbold">Articles en relation 
                                            <span class="badge badge-primary">{{ count($commande->articles) }}</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>                                                
                                        <td class="title--sbold">Nom</td>
                                        <td class="title--sbold">Référence</td>
                                        <td class="title--sbold">Quantité</td>
                                        <td class="title--sbold">Prix unitaire</td>
                                    </tr>
                                    @foreach($commande->articles as $article)                                    
                                        <tr>                                                
                                            <td>{{ $article->nom }}</td>
                                            <td>{{ $article->reference }}</td>
                                            <td>{{ $article->quantite }}</td>                                                                         
                                            <td>{{ $article->prix_unitaire }} &euro;</td>
                                        </tr>
                                    @endforeach   
                                    
                                    <tr>
                                        <td><span class="title--sbold status--denied">Prix</span></td>
                                        
                                        <td colspan="3" class="text-right">
                                            @php $prix = 0;
                                            $prix += $article->quantite * $article->prix_unitaire @endphp
                                            <span class="role admin">{{ $prix }} &euro;</span>
                                        </td>
                                    </tr>
                                @endif

                                <tr class="spacer"></tr>
                                @endforeach                                
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                </div>
            </div>
            
            
        </div>
    </div>
</div>

@endsection