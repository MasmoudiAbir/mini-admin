<!-- HEADER MOBILE-->
<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="index.html">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">                        
                <li>
                    <a href="{{ url('client') }}">
                        <i class="fas fa-users"></i>Clients</a>
                </li>
                <li>
                    <a href="{{ url('commande') }}">
                        <i class="fas fa-table"></i>Commandes</a>
                </li>                        
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE-->

<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            {{ config('app.name', 'Laravel') }}
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">                        
                <li @if(Request::segment(1) == 'client') class="active" @endif>
                    <a href="{{ url('client') }}">
                        <i class="fas fa-users"></i>Clients</a>
                </li>
                <li @if(Request::segment(1) == 'commande') class="active" @endif>
                    <a href="{{ url('commande') }}">
                        <i class="fas fa-table"></i>Commandes</a>
                </li>
                
            </ul>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR-->