@extends('layouts.admin')

@section('content')
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">Clients</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <a href="{{ url('export') }}" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <i class="zmdi zmdi-plus"></i>Exporter toutes les informations
                            </a>                                        
                        </div>
                    </div>
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>                                                
                                    <th>Prénom</th>
                                    <th>Nom</th>
                                    <th>Email</th>
                                    <th>Téléphone</th>
                                    <th>Commandes</th>
                                </tr>
                            </thead>
                            <tbody>                                
                                @foreach($clients as $client)
                                <tr class="tr-shadow">                                                
                                    <td>{{ $client->prenom }}</td>
                                    <td>{{ $client->nom }}</td>
                                    <td>
                                        <span class="block-email">{{ $client->email }}</span>
                                    </td>                                                
                                    <td>{{ $client->telephone }}</td>
                                    <td>{{ $client->commandes_count}}</td>                                                
                                </tr>
                                <tr class="spacer"></tr>
                                @endforeach                                
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                </div>
            </div>
            
            
        </div>
    </div>
</div>

@endsection